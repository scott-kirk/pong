extends KinematicBody2D

export var move_down_action = "ui_down"
export var move_up_action = "ui_up"
export var init_speed = 800 # pixel/sec
export var speed_increase = 1.1
export var speed_cap = 1000
var screen_size
var speed


# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	speed = init_speed


func hit():
	speed = max(speed * speed_increase, speed_cap)


func reset_player_speed():
	speed = init_speed


func _process(delta):
	var velocity = Vector2()
	if Input.is_action_pressed(move_down_action):
		velocity.y += speed
	if Input.is_action_pressed(move_up_action):
		velocity.y -= speed
	move_and_collide(velocity * delta)
