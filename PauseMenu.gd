extends CanvasItem

signal reset_game
signal exit_game
signal resume_game
signal pause_game


func _on_ExitButton_pressed():
	emit_signal("exit_game")


func _on_ResetButton_pressed():
	emit_signal("reset_game")


func _on_ResumeButton_pressed():
	emit_signal("resume_game")
