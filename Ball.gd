extends KinematicBody2D

signal out_of_bounds

export var speed = 700
export var speed_increase = 1.1
export var speed_cap = 1100
var velocity = Vector2()


func launch(dir):
	velocity = Vector2(speed, 0).rotated(dir)


func _physics_process(delta):
	var collision = move_and_collide(velocity * delta)
	if collision:
		velocity = velocity.bounce(collision.normal)
		if collision.collider_velocity.y && velocity.y && velocity.length() < speed_cap:
			velocity *= speed_increase
		if collision.collider.has_method("hit"):
			collision.collider.hit()


func _on_VisibilityNotifier2D_screen_exited():
	emit_signal("out_of_bounds")
