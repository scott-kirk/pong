extends Node

signal start_game


func reset_win():
	$StartButton.hide()
	$WinningLabel.hide()


func player1_won():
	$WinningLabel.text = "Player 1 Wins!"
	$WinningLabel.show()
	$StartButton.show()


func player2_won():
	$WinningLabel.text = "Player 2 Wins!"
	$WinningLabel.show()
	$StartButton.show()


func set_player1_score(score):
	$PlayerScore.text = str(score)


func set_player2_score(score):
	$PlayerScore2.text = str(score)


func _on_StartButton_pressed():
	$StartButton.hide()
	$WinningLabel.hide()
	emit_signal("start_game")
