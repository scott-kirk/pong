extends Node

export var winning_score = 10
var ball_init_pos
var left_side
var rng = RandomNumberGenerator.new()
var player1_score
var player2_score


func _ready():
	$PauseMenu.hide()
	rng.randomize()
	left_side = rng.randi() % 2 == 0
	ball_init_pos = $Ball.position


func _on_Ball_out_of_bounds():
	if $Ball.position.x < ball_init_pos.x:
		player2_score += 1
	else:
		player1_score += 1
	_update_player_scores()
	$Player.reset_player_speed()
	$Player2.reset_player_speed()
	if player1_score >= winning_score:
		$UI.player1_won()
	elif player2_score >= winning_score:
		$UI.player2_won()
	else:
		_launch_ball()


func _launch_ball():
	left_side = !left_side
	$Ball.position = ball_init_pos
	var rotation = PI if left_side else 0
	rotation += rng.randf_range(-PI/4, PI/4)
	$Ball.launch(rotation)


func _update_player_scores():
	$UI.set_player1_score(player1_score)
	$UI.set_player2_score(player2_score)


func _on_UI_start_game():
	$UI.reset_win()
	player1_score = 0
	player2_score = 0
	_update_player_scores()
	_launch_ball()


func _on_PauseMenu_exit_game():
	get_tree().quit()


func _on_PauseMenu_reset_game():
	_on_PauseMenu_resume_game()
	_on_UI_start_game()



func _on_PauseMenu_resume_game():
	get_tree().paused = false
	$PauseMenu.hide()


func _on_PauseMenu_pause_game():
	$PauseMenu.show()
	get_tree().paused = true


func _on_PauseButton_pressed():
	$PauseMenu.show()
	get_tree().paused = true
